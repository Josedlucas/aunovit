
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta charset="utf-8"/>
  <title>Aunovit | Inicio</title>
  <link rel="preconnect" href="https://fonts.googleapis.com"/>
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
  <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&amp;display=swap" rel="stylesheet"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <!-- add favicon-->
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png"/>
  <link rel="manifest" href="img/favicon/site.webmanifest"/>
  <body>
    <header>
      <!---->
      <nav class="navbar navbar-expand-md fixed-top bg-white py-4 py-lg-2 border-bottom">
        <div class="container"><a class="navbar-brand" href="./index.php">
            <embed src="img/logo.svg" type="image/png" alt="Logo"/></a>
          <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="nav nav-pills align-items-center d-lg-flex">
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./empresa.php">Empresa</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./productos.php">Productos</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./marcas.php">Marcas</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./contacto.php">Contacto</a></li>
              <li class="nav-item me-lg-2 me-md-3 mt-4 mt-lg-auto ms-3"><a class="btn btn-outline-primary" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></li>
              <li class="nav-item mt-4 mt-lg-auto ms-3"><a class="btn btn-primary" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <main>
      <section class="one">
        <div class="mb-lg-9 mb-5 pt-lg-6"><img class="img-fluid z-n1 d-none d-md-block" src="img/home/section-one.png"/><img class="img-fluid z-n1 d-md-none d-block" src="img/home/section-one-mobile.png"/>
          <div class="mt-lg-n10 mt-n8">
            <div class="text-center text-white pt-lg-3 d-none d-md-block">
              <h1 class="fw-light">Somos tu mayorista de autopartes <br class="d-none d-lg-block"/></h1>
              <h1 class="fw-semibold">Renault y Volkswagen</h1>
            </div>
            <h1 class="text-center text-white pt-lg-3 d-md-none d-block fw-light">Somos tu mayorista de <br class="d-none d-lg-block"/>
              autopartes <b class="fw-bold">Renault y Volkswagen</b>
            </h1>
          </div>
        </div>
      </section>
      <section class="two container pt-lg-5 col-10 col-lg-12">
        <div class="card border-0 flex-lg-row align-items-center justify-content-center rounded-4 px-lg-4 py-lg-5 py-5">
          <div class="col-lg-4 col-12 d-flex justify-content-center">
            <h5 class="text-center text-lg-start display-8 text-primary d-none d-lg-block fw-semibold">Nuestras Marcas propias</h5>
            <h5 class="text-center text-lg-start display-7 text-primary d-block d-lg-none fw-bolder">Nuestras Marcas propias</h5>
          </div>
          <div class="d-flex justify-content-center mt-5 mt-lg-0 mt-md-auto align-items-center d-lg-flex"><img class="w-55" src="img/marcas/taxim.png"/></div>
          <div class="d-flex justify-content-center mt-5 mt-lg-0 mt-md-auto align-items-center d-lg-flex"><img class="w-55" src="img/marcas/magal.png"/></div>
          <div class="d-flex justify-content-center mt-5 mt-lg-0 mt-md-auto align-items-center d-lg-flex"><img class="w-55" src="img/marcas/nimax.png"/></div>
        </div>
      </section>
      <section class="three mt-lg-n7 mt-xxxl-n4 mt-n9 pt-lg-3">
        <div class="row flex-wrap">
          <div class="col-lg-7 align-items-center d-flex justify-content-center order-md-0 order-1">
            <h5 class="text-center text-lg-center display-6 d-none d-md-block"><span class="text-secondary">Tu partner</span> <br/>
              <span class="text-primary">experto en respuestos</span>
            </h5>
            <h5 class="text-center text-lg-center display-3 d-block d-md-none mt-5"><span class="text-secondary">Tu partner</span> <br/>
              <span class="text-primary">experto en respuestos</span>
            </h5>
          </div>
          <div class="col-lg-5 order-md-1 order-0"><img class="w-100 ms-auto d-none d-md-block" src="img/home/section-three.png"/><img class="w-80 ms-auto d-md-none d-block" src="img/home/section-three-mobile.png"/></div>
        </div>
      </section>
      <section class="four mt-lg-6 container pb-lg-6 mt-6 pb-6">
        <div class="row">
          <div class="col-lg-4 text-center">
            <div class="text-secondary display-4 fw-bold">+  <span class="counter" data-count="1500">0</span> </div>
            
            <div class="text-primary display-9 fw-normal">entregas semanales</div>
          </div>
          <div class="col-lg-4 text-center mt-md-auto mt-5 position-relative rayas-vertical">
            <div class="text-secondary display-4 fw-bold">+ <span class="counter" data-count="90000">0</span></div>
            
            <div class="text-primary display-9 fw-normal">procuctos</div>
          </div>
          <div class="col-lg-4 text-center mt-md-auto mt-5">
            <div class="text-secondary display-4 fw-bold">+ <span class="counter" data-count="800">0</span></div>
            
            <div class="text-primary display-9 fw-normal">Clientes felices</div>
          </div>
        </div>
      </section>
      <section class="container mt-lg-5 mb-lg-7">
        <div class="owl1 owl-carousel">
          <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4"><img class="card-img-top rounded-top-4" src="img/slider-image/img.png" alt="..."/>
            <div class="card-body">
              <h5 class="card-title text-primary fw-normal display-8">Variedad de Stock</h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
              <p class="card-text fs-6 fw-light">
                La cantidad de artículos por marca así como el volumen de stock nos diferencias en la oferta, resolviendo todo el abanico de necesidades en un solo lugar.
                <br/>
                <br/>
                <br/>
              </p>
            </div>
          </div>
          <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4"><img class="card-img-top rounded-top-4" src="img/slider-image/img_1.png" alt="..."/>
            <div class="card-body">
              <h5 class="card-title text-primary fw-normal display-8">Atención personalizada</h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
              <p class="card-text fs-6 fw-light">
                Construimos relaciones sólidas con cada cliente, respondiendo siempre a todas las inquietudes, asesorando de cerca y ofreciendo las mejores soluciones.
                <br/>
                <br/>
                <br/>
              </p>
            </div>
          </div>
          <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4"><img class="card-img-top rounded-top-4" src="img/slider-image/img_2.png" alt="..."/>
            <div class="card-body">
              <h5 class="card-title text-primary fw-normal display-8">Planta especializada</h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
              <p class="card-text fs-6 fw-light">En Aunovit contamos una planta diseñada específicamente para responder en tiempo y forma con los pedidos y las ventas a lo largo y ancho del país. Contamos con un equipo de más de 200 personas para garantizar el éxito en todo el proceso.</p>
            </div>
          </div>
        </div>
      </section><br class="d-block d-lg-none"/><br class="d-block d-lg-none"/><br class="d-block d-lg-none"/>
      <section class="container mt-lg-7 mb-lg-7 col-11 col-md-12 px-3 px-md-auto mb-5">
        <div class="card border-0 px-lg-6 py-lg-5 rounded-4 px-4 py-5">
          <div class="row">
            <div class="col-lg-3 col-12"><img src="icon/service_1.svg"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Entrega diaria <br class="d-none d-lg-block"/>
                asegurada
              </h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
              <p class="fw-light">Nuestro equipo de logística trabaja contrarreloj para despachar los pedidos recibidos en el mismo día, eso fortalece la calidad de nuestro servicio.</p>
            </div>
            <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_2.svg"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Servicio paragolpe <br class="d-none d-lg-block"/>
                a paragolpe
              </h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
              <p class="fw-light">Contamos con una variedad de productos que resuelven todas las demandas del tu mostrador.</p>
            </div>
            <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_3.svg"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Cobertura nacional <br class="d-none d-lg-block"/>
                <br/>
              </h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
              <p class="fw-light">Despachamos pedidos a cualquier punto del país en tiempos super optimizados.</p>
            </div>
            <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_4.svg"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Precio competitivo <br class="d-none d-lg-block"/>
                en toda categoría
              </h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
              <p class="fw-light">Nuestro objetivo es brindar el mejor servicio, calidad y soluciones para tu mostrador. <br class="d-none d-lg-block"/>
                Eso se logra brindando el mejor precio del mercado.
              </p>
            </div>
          </div>
        </div>
      </section>
      <section class="seven mt-lg-n8 z-1 pb-lg-6 pb-6"><img class="img-fluid w-55 mb-lg-7 ms-auto d-none d-md-block" src="img/home/section-seven.png"/>
        <div class="container mt-lg-n10 pt-lg-6 pt-6">
          <div class="owl2 owl-carousel">
            <div class="testimonials-item d-flex col-lg-9 flex-wrap col-11">
              <div class="col-lg-8 pe-lg-6 order-lg-0 order-1">
                <div class="header-testimonials d-flex mt-5 mt-md-0">
                  <h5 class="text-secondary fw-bold display-9">
                    <div class="count">1/2</div>
                  </h5>
                  <h5 class="ms-3 text-white fw-bold display-9">REPUESTOS SANTA ROSA (LA PAMPA)</h5>
                </div>
                <div class="header-body text-white fw-light mt-lg-4 mt-4">
                  En Repuestos Santa Rosa comenzamos a trabajar fuerte con ésta empresa comenzando la pandemia y gracias a ellos pudimos seguir trabajando y creciendo día a día debido a su política empresarial donde nunca nos soltaron la mano, nunca cortaron la disponibilidad de repuestos, pudiendo siempre trabajar con previsibilidad, y buscando el mejor negocio para ambos.
                  <br/>
                  <br/>
                  Respecto al capital humano, no puedo más que decir que unos genios todos, desde ventas hasta el viejo Snaider, pasando por sus hijos y Gonza. Siempre dispuestos a encontrar una solución y seguir creciendo junto a sus clientes. Y respecto a los productos una ventaja que cuenten con originales, primeras marcas y las propias, toda la gama de calidades y precios. Gracias por acompañarnos ahora y siempre
                </div>
              </div>
              <div class="col-lg-4 align-items-end d-lg-flex order-lg-1 order-0"><img class="w-100 d-none d-lg-block" src="img/testimonials/img.png"/><img class="w-80 d-block d-lg-none mx-auto" src="img/testimonials/img.png"/></div>
            </div>
            <div class="testimonials-item d-flex col-lg-9 flex-wrap col-11">
              <div class="col-lg-8 pe-lg-6 order-lg-0 order-1">
                <div class="header-testimonials d-flex mt-5 mt-md-0">
                  <h5 class="text-secondary fw-bold display-9">
                    <div class="count">2/2</div>
                  </h5>
                  <h5 class="ms-3 text-white fw-bold display-9">RENAULT SUR (POSADAS-MISIONES)</h5>
                </div>
                <div class="header-body text-white fw-light mt-lg-4 mt-4">En Renault Sur contamos con la línea completa de los excelente productos Taxim, y destacamos la tan amable y cálida atención de todo el equipo de Aunovit.</div>
              </div>
              <div class="col-lg-4 align-items-end d-lg-flex order-lg-1 order-0"><img class="w-100 d-none d-lg-block" src="img/testimonials/img_1.png"/><img class="w-80 d-block d-lg-none mx-auto" src="img/testimonials/img_1.png"/></div>
            </div>
          </div>
        </div>
      </section>
      <section class="eight d-lg-flex pb-lg-7"><img class="w-50 d-none d-lg-block" src="img/home/section-eight.png"/><img class="w-100 d-block d-lg-none mx-auto" src="img/home/section-eight-mobile.png"/>
        <div class="col-lg-6 ms-lg-auto align-items-center d-flex justify-content-center pt-lg-6">
          <div>
            <h2 class="text-primary display-6 text-center d-none d-lg-block">Soluciona todos tus <br class="d-none d-lg-block"/> pedidos con aunovit.</h2>
            <h2 class="text-primary display-3 text-center d-block d-lg-none mt-5">Soluciona todos tus <br class="d-none d-lg-block"/> pedidos con aunovit.</h2>
            <div class="d-flex justify-content-center mt-4"><a class="btn btn-secondary">Solicitar cuenta<i class="bi bi-arrow-right ms-2"></i></a></div>
          </div>
        </div>
      </section>
    </main>
    <script src="js/jquery.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
    <script src="js/jquery.validate.js"> </script>
    <script src="js/main.js"></script>
    <div class="top-footer">
      <footer class="container py-6">
        <div class="row">
          <div class="col-md-6 col-lg-4 col-12 d-md-block d-flex justify-content-center"><a href="./index.php"><img src="img/logo.svg" alt="Logo"/></a></div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./empresa.php">Empresa</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./productos.php">Productos</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./marcas.php">Marcas</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./contacto.php">Contacto</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-3"><a class="nav-link p-0" href="mailto:ventasmayor@aunovit.com"><img class="me-3" src="icon/email.png" alt="Email" width="25"/>ventasmayor@aunovit.com</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="tel:+5491170780416"><img class="me-3" src="icon/audifono.png" alt="Phone" width="25"/>+54 9 11 7078 0416</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="https://wa.me/5491168581006"><img class="me-3" src="icon/whatsapp.png" alt="Whatsapp" width="25"/>+54 9 11 6858 1006</a></li>
            </ul>
          </div>
          <div class="col-lg-2 col-8 mx-auto mt-2 mt-lg-0"><a class="btn btn-primary mt-lg-4 mt-4" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a><a class="btn btn-outline-primary mt-lg-4 mt-4" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></div>
        </div>
      </footer>
    </div>
  </body>
</html>