
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta charset="utf-8"/>
  <title>Aunovit | Catalogo</title>
  <link rel="preconnect" href="https://fonts.googleapis.com"/>
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
  <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&amp;display=swap" rel="stylesheet"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <!-- add favicon-->
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png"/>
  <link rel="manifest" href="img/favicon/site.webmanifest"/>
  <body>
    <header>
      <!---->
      <nav class="navbar navbar-expand-md fixed-top bg-white py-4 py-lg-2 border-bottom">
        <div class="container"><a class="navbar-brand" href="./index.php">
            <embed src="img/logo.svg" type="image/png" alt="Logo"/></a>
          <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="nav nav-pills align-items-center d-lg-flex">
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./empresa.php">Empresa</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./productos.php">Productos</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./marcas.php">Marcas</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./contacto.php">Contacto</a></li>
              <li class="nav-item me-lg-2 me-md-3 mt-4 mt-lg-auto ms-3"><a class="btn btn-outline-primary" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></li>
              <li class="nav-item mt-4 mt-lg-auto ms-3"><a class="btn btn-primary" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <main>
      <section class="catatalogo-one bg-primary">
        <div class="container py-lg-8 pb-5 pt-7">
          <div class="row">
            <div class="col-lg-6 order-lg-0 order-1"><img class="w-80 mx-auto d-lg-flex d-block" src="img/catalogo/section-one.png"/></div>
            <div class="col-lg-6 align-items-center d-lg-flex order-lg-1 order-0">
              <div>
                <h1 class="text-white display-6 text-start d-none d-lg-block">Catálogo digital, <br/>
                  CASAPARTS
                </h1>
                <h1 class="text-white display-5 text-center text-lg-start d-block d-lg-none mt-4">Catálogo digital, <br/>
                  CASAPARTS
                </h1>
                <h2 class="display-8 text-white my-lg-4 text-center my-4 text-lg-start">En constante renovación Casaparts brinda la información necesaria para que tu mostrador sea ágil, con precios seguros y competitivos.</h2>
                <div class="text-white text-center text-lg-start">Cada producto consultado ofrece fotos de distintos ángulos para la búsqueda correcta de artículos, consultar stock, precio actualizado segundo a segundo, consulta de cta cte y envío de pedidos con un solo click.</div>
                <div class="mt-lg-5 mb-lg-0 mb-5 justify-content-center d-flex mt-5 justify-content-lg-start"><a class="btn btn-outline-light" href="./contacto.php">Dejanos tus datos<i class="bi bi-arrow-right ms-2"></i></a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="container mt-lg-7 mb-lg-7 col-11 col-md-12 px-3 px-md-auto mb-5 mt-lg-n7 mt-5">
        <div class="card border-0 px-lg-6 py-lg-5 rounded-4 px-4 py-5 d-none d-md-block">
          <h1 class="text-center text-primary display-7 mb-lg-6 fw-normal">En nuestro catálogo Autoparts va a tener todo en 1 solo lugar:</h1>
          <div class="row">
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/tag.png"/>
              <h5 class="text-primary display-9 mt-lg-4 mt-3 text-center fw-normal">Precios <br/>
                actualizados
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/image-product.png"/>
              <h5 class="text-primary display-9 mt-lg-4 mt-3 text-center fw-normal">Imágenes de <br/>
                cada producto
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/lupa.png"/>
              <h5 class="text-primary display-9 mt-lg-4 mt-3 text-center fw-normal">Búsqueda <br/>
                inteligente
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/megafono.png"/>
              <h5 class="text-primary display-9 mt-lg-4 mt-3 text-center fw-normal">Banners <br/>
                informativos
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/papel.png"/>
              <h5 class="text-primary display-9 mt-lg-4 mt-3 text-center fw-normal">Novedades <br class="d-none d-lg-block"/>
                diarias
              </h5>
            </div>
          </div>
        </div>
        <div class="border-0 px-lg-6 py-lg-5 rounded-4 px-4 py-4 d-block d-md-none">
          <h1 class="text-center text-primary display-7 mb-lg-6">En nuestro catálogo Autoparts va a tener todo en 1 solo lugar:</h1>
          <div class="row">
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/tag.png"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 text-center">Precios <br/>
                actualizados
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/image-product.png"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 text-center">Imágenes de <br/>
                cada producto
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/lupa.png"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 text-center">Búsqueda <br/>
                inteligente
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/megafono.png"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 text-center">Banners <br/>
                informativos
              </h5>
            </div>
            <div class="col-lg col-12 mt-5 mt-lg-0"><img class="mx-auto d-block w-30" src="icon/papel.png"/>
              <h5 class="text-primary display-8 mt-lg-4 mt-3 text-center">Novedades <br class="d-none d-lg-block"/>
                diarias
              </h5>
            </div>
          </div>
        </div>
      </section>
    </main>
    <script src="js/jquery.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
    <script src="js/jquery.validate.js"> </script>
    <script src="js/main.js"></script>
    <div class="top-footer">
      <footer class="container py-6">
        <div class="row">
          <div class="col-md-6 col-lg-4 col-12 d-md-block d-flex justify-content-center"><a href="./index.php"><img src="img/logo.svg" alt="Logo"/></a></div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./empresa.php">Empresa</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./productos.php">Productos</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./marcas.php">Marcas</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./contacto.php">Contacto</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-3"><a class="nav-link p-0" href="mailto:ventasmayor@aunovit.com"><img class="me-3" src="icon/email.png" alt="Email" width="25"/>ventasmayor@aunovit.com</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="tel:+5491170780416"><img class="me-3" src="icon/audifono.png" alt="Phone" width="25"/>+54 9 11 7078 0416</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="https://wa.me/5491168581006"><img class="me-3" src="icon/whatsapp.png" alt="Whatsapp" width="25"/>+54 9 11 6858 1006</a></li>
            </ul>
          </div>
          <div class="col-lg-2 col-8 mx-auto mt-2 mt-lg-0"><a class="btn btn-primary mt-lg-4 mt-4" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a><a class="btn btn-outline-primary mt-lg-4 mt-4" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></div>
        </div>
      </footer>
    </div>
  </body>
</html>