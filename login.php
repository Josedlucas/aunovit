<?
session_start();  
if ($_SESSION["autentif"] == "SI" && isset($_SESSION["autentif"])) {
	header("Location:privado_index.php");
	exit();
}
include("lib.php");
?>
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta charset="utf-8"/>
  <title></title>
  <link rel="preconnect" href="https://fonts.googleapis.com"/>
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
  <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&amp;display=swap" rel="stylesheet"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <!-- add favicon-->
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png"/>
  <link rel="manifest" href="img/favicon/site.webmanifest"/>
  <body>
    <main>
      <section class="login"><img class="vw-100 vh-100 d-md-block d-none position-absolute" src="img/login/login.png" alt="" srcset="" style="object-fit: cover; object-position: center; top: 0; bottom: 0"/>
        <div class="card shadow-none col-lg-4 mx-lg-auto rounded-5 py-lg-5 px-lg-5 border-0 col-10 mx-auto mt-lg-6">
          <div class="card-header border-0 pt-lg-0 pt-5 bg-white"><img class="mx-auto d-block" src="img/logo.svg"/>
            <p class="text-center mt-lg-4 mt-4">Accede aquí para decargar la <br/>
              lista de precios:
            </p>
          </div>
          <div class="card-body">
            <form class="form" action="login_control.php" method="post">
              <div class="input-group input-group-lg"><span class="input-group-text"><img src="icon/user.png" width="20"/></span>
                <input class="form-control" type="text" name="usuario" placeholder="Usuario"/>
              </div>
              <div class="input-group input-group-lg mt-lg-4 mt-3"><span class="input-group-text"><img src="icon/key.png" width="20"/></span>
                <input class="form-control" type="password" name="password" placeholder="Contraseña"/>
              </div>
              <div class="justify-content-center d-flex mt-lg-5 mt-5">
                <button class="btn btn-primary" type="submit">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></button>
              </div>
            </form>
          </div>
        </div>
      </section>
    </main>
    <script src="node_modules/jquery/dist/jquery.js"></script>
    <script src="node_modules/owl.carousel/dist/owl.carousel.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>