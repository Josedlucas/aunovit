
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta charset="utf-8"/>
  <title>Aunovit | Empresa</title>
  <link rel="preconnect" href="https://fonts.googleapis.com"/>
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
  <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&amp;display=swap" rel="stylesheet"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <!-- add favicon-->
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png"/>
  <link rel="manifest" href="img/favicon/site.webmanifest"/>
  <body>
    <header>
      <!---->
      <nav class="navbar navbar-expand-md fixed-top bg-white py-4 py-lg-2 border-bottom">
        <div class="container"><a class="navbar-brand" href="./index.php">
            <embed src="img/logo.svg" type="image/png" alt="Logo"/></a>
          <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="nav nav-pills align-items-center d-lg-flex">
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link active" href="./empresa.php">Empresa</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./productos.php">Productos</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./marcas.php">Marcas</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./contacto.php">Contacto</a></li>
              <li class="nav-item me-lg-2 me-md-3 mt-4 mt-lg-auto ms-3"><a class="btn btn-outline-primary" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></li>
              <li class="nav-item mt-4 mt-lg-auto ms-3"><a class="btn btn-primary" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <main>
      <section class="empresa-one mb-lg-6 mb-6"><img class="w-50 ms-auto d-none d-md-block mt-lg-8" src="img/empresa/section-one.png"/><img class="w-100 ms-auto d-block d-md-none mt-7 pt-5" src="img/empresa/section-one-mobile.png"/>
        <div class="mt-lg-n10 mt-n9">
          <h1 class="text-center text-primary pt-lg-3 d-none d-md-block fw-light">Aunovit, llega a tu mostrador para <br class="d-none d-lg-block"/>
            garantizar el <span class="text-secondary">servicio de un especialista.</span>
          </h1>
          <h1 class="text-center text-primary pt-lg-3 d-md-none d-block fw-light mb-5 display-4 px-4">Aunovit, llega a tu mostrador para <br class="d-none d-lg-block"/>
            garantizar el <span class="text-secondary">servicio de un especialista.</span>
          </h1>
          <div class="row col-lg-8 mx-auto mt-lg-6">
            <div class="col-lg-6 text-primary fw-light text-center text-lg-start col-10 mx-auto">
              <div class="display-8 fw-bolder">
                Contamos con un moderno centro de distribución 6000mts2 para cumplir a diario las necesidades de
                nuestros clientes y más de 25000mts2 en depósitos.
              </div><br/>
              Somos especialistas y nos enfocamos en cubrir el 100% de las demandas de las marcas que representamos.
            </div>
            <div class="col-lg-6 text-primary fw-light text-center text-lg-start mt-3 mt-lg-0 col-10 mx-auto">
              Contamos con la distribución oficial y exclusiva de las marcas Taxim y Nimax, hoy posicionadas las marcas más importantes
              del mercado aftermarket para Renault y Volkswagen, acompañadas de las principales marcas de repuestos que necesita tu mostrador
              con el mejor precio garantizado.
              <br/>
              <br/>
              Si sos especialista en Renault o Volkswagen necesitás un especialista como Aunovit.
            </div>
          </div>
        </div>
      </section>
      <section class="empresa-two">
        <div class="mb-lg-9 mb-5 pb-6"><img class="img-fluid z-n1 d-none d-md-block" src="img/empresa/section-two.png"/><img class="img-fluid z-n1 d-md-none d-block" src="img/empresa/section-two-mobile.png"/>
          <div class="mt-lg-n10 mt-n10 pt-6">
            <h1 class="text-center text-white pt-lg-3 d-none d-md-block fw-light">¿Todavía no sos cliente? <br class="d-none d-lg-block"/>
              Contactá con nosotros.
            </h1>
            <h1 class="text-center text-white pt-lg-3 d-md-none d-block fw-light display-3">¿Todavía no sos <br/> cliente? <br/>
              Contactá con nosotros.
            </h1>
            <div class="d-flex justify-content-center mt-5"><a class="btn btn-secondary" href="./contacto.php">Contactanos aquí<i class="bi bi-arrow-right ms-2"></i></a></div>
          </div>
        </div>
      </section>
      <div class="mt-lg-n9 mt-n5">
        <section class="container mt-lg-7 mb-lg-7 col-11 col-md-12 px-3 px-md-auto mb-5">
          <div class="card border-0 px-lg-6 py-lg-5 rounded-4 px-4 py-5">
            <div class="row">
              <div class="col-lg-3 col-12"><img src="icon/service_1.svg"/>
                <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Entrega diaria <br class="d-none d-lg-block"/>
                  asegurada
                </h5>
                <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                <p class="fw-light">Nuestro equipo de logística trabaja contrarreloj para despachar los pedidos recibidos en el mismo día, eso fortalece la calidad de nuestro servicio.</p>
              </div>
              <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_2.svg"/>
                <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Servicio paragolpe <br class="d-none d-lg-block"/>
                  a paragolpe
                </h5>
                <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                <p class="fw-light">Contamos con una variedad de productos que resuelven todas las demandas del tu mostrador.</p>
              </div>
              <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_3.svg"/>
                <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Cobertura nacional <br class="d-none d-lg-block"/>
                  <br/>
                </h5>
                <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                <p class="fw-light">Despachamos pedidos a cualquier punto del país en tiempos super optimizados.</p>
              </div>
              <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_4.svg"/>
                <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Precio competitivo <br class="d-none d-lg-block"/>
                  en toda categoría
                </h5>
                <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                <p class="fw-light">Nuestro objetivo es brindar el mejor servicio, calidad y soluciones para tu mostrador. <br class="d-none d-lg-block"/>
                  Eso se logra brindando el mejor precio del mercado.
                </p>
              </div>
            </div>
          </div>
        </section>
      </div><br class="d-block d-lg-none"/><br class="d-block d-lg-none"/><br class="d-block d-lg-none"/>
      <section class="container mt-lg-5 mb-lg-7">
        <div class="owl1 owl-carousel">
          <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4"><img class="card-img-top rounded-top-4" src="img/slider-image/img.png" alt="..."/>
            <div class="card-body">
              <h5 class="card-title text-primary fw-normal display-8">Variedad de Stock</h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
              <p class="card-text fs-6 fw-light">
                La cantidad de artículos por marca así como el volumen de stock nos diferencias en la oferta, resolviendo todo el abanico de necesidades en un solo lugar.
                <br/>
                <br/>
                <br/>
              </p>
            </div>
          </div>
          <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4"><img class="card-img-top rounded-top-4" src="img/slider-image/img_1.png" alt="..."/>
            <div class="card-body">
              <h5 class="card-title text-primary fw-normal display-8">Atención personalizada</h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
              <p class="card-text fs-6 fw-light">
                Construimos relaciones sólidas con cada cliente, respondiendo siempre a todas las inquietudes, asesorando de cerca y ofreciendo las mejores soluciones.
                <br/>
                <br/>
                <br/>
              </p>
            </div>
          </div>
          <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4"><img class="card-img-top rounded-top-4" src="img/slider-image/img_2.png" alt="..."/>
            <div class="card-body">
              <h5 class="card-title text-primary fw-normal display-8">Planta especializada</h5>
              <div class="raya w-20 mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
              <p class="card-text fs-6 fw-light">En Aunovit contamos una planta diseñada específicamente para responder en tiempo y forma con los pedidos y las ventas a lo largo y ancho del país. Contamos con un equipo de más de 200 personas para garantizar el éxito en todo el proceso.</p>
            </div>
          </div>
        </div>
      </section>
    </main>
    <script src="js/jquery.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
    <script src="js/jquery.validate.js"> </script>
    <script src="js/main.js"></script>
    <div class="top-footer">
      <footer class="container py-6">
        <div class="row">
          <div class="col-md-6 col-lg-4 col-12 d-md-block d-flex justify-content-center"><a href="./index.php"><img src="img/logo.svg" alt="Logo"/></a></div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./empresa.php">Empresa</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./productos.php">Productos</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./marcas.php">Marcas</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./contacto.php">Contacto</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-3"><a class="nav-link p-0" href="mailto:ventasmayor@aunovit.com"><img class="me-3" src="icon/email.png" alt="Email" width="25"/>ventasmayor@aunovit.com</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="tel:+5491170780416"><img class="me-3" src="icon/audifono.png" alt="Phone" width="25"/>+54 9 11 7078 0416</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="https://wa.me/5491168581006"><img class="me-3" src="icon/whatsapp.png" alt="Whatsapp" width="25"/>+54 9 11 6858 1006</a></li>
            </ul>
          </div>
          <div class="col-lg-2 col-8 mx-auto mt-2 mt-lg-0"><a class="btn btn-primary mt-lg-4 mt-4" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a><a class="btn btn-outline-primary mt-lg-4 mt-4" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></div>
        </div>
      </footer>
    </div>
  </body>
</html>