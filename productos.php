
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta charset="utf-8"/>
  <title>Aunovit | Producto</title>
  <link rel="preconnect" href="https://fonts.googleapis.com"/>
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
  <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&amp;display=swap" rel="stylesheet"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <!-- add favicon-->
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png"/>
  <link rel="manifest" href="img/favicon/site.webmanifest"/>
  <body>
    <header>
      <!---->
      <nav class="navbar navbar-expand-md fixed-top bg-white py-4 py-lg-2 border-bottom">
        <div class="container"><a class="navbar-brand" href="./index.php">
            <embed src="img/logo.svg" type="image/png" alt="Logo"/></a>
          <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="nav nav-pills align-items-center d-lg-flex">
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./empresa.php">Empresa</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link active" href="./productos.php">Productos</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./marcas.php">Marcas</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./contacto.php">Contacto</a></li>
              <li class="nav-item me-lg-2 me-md-3 mt-4 mt-lg-auto ms-3"><a class="btn btn-outline-primary" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></li>
              <li class="nav-item mt-4 mt-lg-auto ms-3"><a class="btn btn-primary" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <main>
      <section class="productos-one flex-wrap d-flex d-lg-block"><img class="w-100 mt-lg-6 pt-2 d-lg-block d-none order-1 order-lg-0" src="img/productos/section-one.png"/><img class="w-100 mt-lg-6 pt-lg-2 mt-4 d-lg-none d-block order-1 order-lg-0" src="img/productos/section-one-mobile.png"/>
        <div class="col-lg-5 me-auto mt-lg-n10 pt-lg-1 order-lg-1 order-0 mt-7 pt-4">
          <h1 class="text-center text-primary display-4 pt-lg-4 d-lg-block d-none">De paragolpe a <br/>
            paragolpe
          </h1>
          <h1 class="text-center text-primary display-4 pt-lg-4 d-block d-lg-none">De paragolpe a <br/>
            paragolpe
          </h1>
          <div class="display-9 text-primary text-center mt-4 mt-lg-5 px-lg-0 px-5 lh-sm">Más de <b class="fw-bold">30.000</b> referencias en <b class="fw-bold">stock continuo</b> <br class="d-none d-lg-block"/>
            y la representación de marcas líderes en el <br class="d-none d-lg-block"/>
            mercado en nuestro catálogo
          </div>
          <div class="justify-content-center d-flex mt-4 mt-lg-5 d-lg-flex d-none"><a class="btn btn-secondary" href="./contacto.php">Solicitar cuenta<i class="bi bi-arrow-right ms-2"></i></a></div>
        </div>
      </section>
      <section class="productos-two">
        <div class="mt-lg-8 pt-lg-5 mt-5">
          <h1 class="text-center text-primary pt-lg-3 d-none d-md-block fw-light">Nuestras líneas de productos para Renault y VW <br class="d-none d-lg-block"/>
            <span class="text-secondary">abarcan la totalidad de tus necesidades.</span>
          </h1>
          <h1 class="text-center text-primary pt-lg-3 d-md-none d-block fw-light mb-5 display-4 px-4">
            Nuestras líneas de productos para Renault y VW
            <span class="text-secondary">abarcan la totalidad de tus necesidades.</span>
          </h1>
          <div class="display-8 fw-normal text-center text-primary mt-lg-4">Contamos con más de 90 productos en stock permanente  <br class="d-none d-lg-block"/>
            para satisfacer todas tus necesidades de autopartes.
          </div>
          <div class="display-6 fw-normal text-center text-primary mt-lg-6 mt-5">Aunovit lo tiene.</div>
        </div>
      </section>
      <div class="mt-lg-n7 z-n1 position-relative mt-n8"><img class="w-60 ms-lg-7 d-lg-block d-none" src="img/productos/section-three.png"/><img class="w-80 ms-lg-7 d-block d-lg-none mt-3" src="img/productos/section-three-mobile.png"/>
        <div class="mt-lg-n11 pt-lg-8">
          <section class="container mt-lg-7 mb-lg-7 col-11 col-md-12 px-3 px-md-auto mb-5">
            <div class="card border-0 px-lg-6 py-lg-5 rounded-4 px-4 py-5">
              <div class="row">
                <div class="col-lg-3 col-12"><img src="icon/service_1.svg"/>
                  <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Entrega diaria <br class="d-none d-lg-block"/>
                    asegurada
                  </h5>
                  <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                  <p class="fw-light">Nuestro equipo de logística trabaja contrarreloj para despachar los pedidos recibidos en el mismo día, eso fortalece la calidad de nuestro servicio.</p>
                </div>
                <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_2.svg"/>
                  <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Servicio paragolpe <br class="d-none d-lg-block"/>
                    a paragolpe
                  </h5>
                  <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                  <p class="fw-light">Contamos con una variedad de productos que resuelven todas las demandas del tu mostrador.</p>
                </div>
                <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_3.svg"/>
                  <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Cobertura nacional <br class="d-none d-lg-block"/>
                    <br/>
                  </h5>
                  <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                  <p class="fw-light">Despachamos pedidos a cualquier punto del país en tiempos super optimizados.</p>
                </div>
                <div class="col-lg-3 col-12 mt-4 mt-md-0"><img src="icon/service_4.svg"/>
                  <h5 class="text-primary display-8 mt-lg-4 mt-3 fw-normal">Precio competitivo <br class="d-none d-lg-block"/>
                    en toda categoría
                  </h5>
                  <div class="raya w-20 mb-lg-4 mt-lg-3 my-4"></div>
                  <p class="fw-light">Nuestro objetivo es brindar el mejor servicio, calidad y soluciones para tu mostrador. <br class="d-none d-lg-block"/>
                    Eso se logra brindando el mejor precio del mercado.
                  </p>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </main>
    <script src="js/jquery.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
    <script src="js/jquery.validate.js"> </script>
    <script src="js/main.js"></script>
    <div class="top-footer">
      <footer class="container py-6">
        <div class="row">
          <div class="col-md-6 col-lg-4 col-12 d-md-block d-flex justify-content-center"><a href="./index.php"><img src="img/logo.svg" alt="Logo"/></a></div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./empresa.php">Empresa</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./productos.php">Productos</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./marcas.php">Marcas</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./contacto.php">Contacto</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-3"><a class="nav-link p-0" href="mailto:ventasmayor@aunovit.com"><img class="me-3" src="icon/email.png" alt="Email" width="25"/>ventasmayor@aunovit.com</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="tel:+5491170780416"><img class="me-3" src="icon/audifono.png" alt="Phone" width="25"/>+54 9 11 7078 0416</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="https://wa.me/5491168581006"><img class="me-3" src="icon/whatsapp.png" alt="Whatsapp" width="25"/>+54 9 11 6858 1006</a></li>
            </ul>
          </div>
          <div class="col-lg-2 col-8 mx-auto mt-2 mt-lg-0"><a class="btn btn-primary mt-lg-4 mt-4" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a><a class="btn btn-outline-primary mt-lg-4 mt-4" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></div>
        </div>
      </footer>
    </div>
  </body>
</html>