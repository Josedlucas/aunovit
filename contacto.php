<html>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta charset="utf-8"/>
<title>Aunovit | Contacto</title>
<link rel="preconnect" href="https://fonts.googleapis.com"/>
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
<link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&amp;display=swap" rel="stylesheet"/>
<link href="css/sweetalert2.css" rel="stylesheet"/>
<link href="css/bootstrap.css" rel="stylesheet"/>
<!-- add favicon-->
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png"/>
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png"/>
<link rel="manifest" href="img/favicon/site.webmanifest"/>
<body>
<header>
    <!---->
    <nav class="navbar navbar-expand-md fixed-top bg-white py-4 py-lg-2 border-bottom">
        <div class="container"><a class="navbar-brand" href="./index.php">
                <embed src="img/logo.svg" type="image/png" alt="Logo"/></a>
            <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="nav nav-pills align-items-center d-lg-flex">
                    <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./empresa.php">Empresa</a></li>
                    <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./productos.php">Productos</a></li>
                    <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./marcas.php">Marcas</a></li>
                    <li class="nav-item me-lg-2 position-relative"><a class="nav-link active" href="./contacto.php">Contacto</a></li>
                    <li class="nav-item me-lg-2 me-md-3 mt-4 mt-lg-auto ms-3"><a class="btn btn-outline-primary" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></li>
                    <li class="nav-item mt-4 mt-lg-auto ms-3"><a class="btn btn-primary" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<main>
    <section class="contacto-one bg-primary"><img class="w-100 mx-auto d-lg-block d-none" src="img/contacto/section-one.png"/><img class="w-100 mx-auto d-lg-none d-block" src="img/contacto/section-one-mobile.png"/></section>
    <section class="container mt-lg-7 mb-lg-7 col-11 col-md-12 px-3 px-md-auto mb-1 mt-lg-n7 mt-5">
        <div class="mt-lg-n9 mt-n9 pt-lg-0 pt-5">
            <h1 class="text-white text-center">Contacto</h1>
            <p class="text-white text-center mb-5 mb-lg-6 display-9">
                Completa el formulario a continuación o comunícate <br class="d-none d-lg-block"/>
                por los otros canales de contacto.
            </p>
        </div>
        <div class="card border-0 px-lg-6 py-lg-5 rounded-4 px-4 py-4 shadow-none">
            <div class="row"><a class="text-decoration-none col-lg col-12 mt-5 mt-lg-0 text-center mb-lg-0 mb-5" href="tel:+5491170780416"><img class="mx-auto d-block mb-lg-4 mb-3" src="icon/audifono.png"/><small class="text-secondary display-10 fw-normal">Línea de teléfono</small>
                    <h5 class="text-primary display-8 mt-lg-2 fw-normal"> +54 91 17078 0416</h5></a><a class="text-decoration-none col-lg col-12 mt-5 mt-lg-0 text-center d-lg-block d-none" href="https://wa.me/5491168581006"><img class="mx-auto d-block mb-lg-4" src="icon/whatsapp.png"/><small class="text-secondary display-10 fw-normal">Whatsapp</small>
                    <h5 class="text-primary display-8 mt-lg-2 fw-normal">+54 91 16858 1006</h5></a><a class="text-decoration-none col-lg col-12 mt-5 mt-lg-0 text-center d-lg-block d-none" href="mailto:ventasmayor@aunovit.com"><img class="mx-auto d-block mb-lg-4" src="icon/email.png"/><small class="text-secondary display-10 fw-normal">Correo electrónico</small>
                    <h5 class="text-primary display-8 mt-lg-2 fw-normal">ventasmayor@aunovit.com</h5></a></div>
            <form class="row pt-lg-6" action="#" id="contact">
                <div class="col-lg-6 form-floating mt-3">
                    <input class="form-control" type="text" id="name" name="name" placeholder="Nombre y apellido"/>
                    <label class="ms-lg-3 text-muted opacity-50 ms-3 fw-ligh" for="name">Nombre y apellido</label>
                </div>
                <div class="col-lg-6 form-floating mt-3">
                    <input class="form-control" type="text" id="business" name="business" placeholder="Negocio / Local"/>
                    <label class="ms-lg-3 text-muted opacity-50 ms-3 fw-ligh" for="business">Negocio / Local</label>
                </div>
                <div class="col-lg-6 form-floating mt-3">
                    <input class="form-control" type="tel" id="phone" name="phone" placeholder="Teléfono"/>
                    <label class="ms-lg-3 text-muted opacity-50 ms-3 fw-ligh" for="phone">Teléfono</label>
                </div>
                <div class="col-lg-6 form-floating mt-3">
                    <input class="form-control" type="email" id="email" name="email" placeholder="Correo electrónico"/>
                    <label class="ms-lg-3 text-muted opacity-50 ms-3 fw-ligh" for="email">Correo electrónico</label>
                </div>
                <div class="col-lg-6 form-floating mt-3">
                    <input class="form-control" type="text" id="address1" name="address1" placeholder="Localidad"/>
                    <label class="ms-lg-3 text-muted opacity-50 ms-3 fw-ligh" for="address1">Localidad</label>
                </div>
                <div class="col-lg-6 form-floating mt-3">
                    <input class="form-control" type="text" id="address2" name="address2" placeholder="Provincia"/>
                    <label class="ms-lg-3 text-muted opacity-50 ms-3 fw-ligh" for="address2">Provincia</label>
                </div>
                <div class="col-lg-6 form-floating mt-3">
                    <textarea class="form-control" type="text" id="message" name="message" placeholder="Asunto" style="min-height: 150px"></textarea>
                    <label class="ms-lg-3 text-muted opacity-50 ms-3 fw-ligh" for="message">Mensaje</label>
                </div>
                <div class="col-lg-6">
                    <p class="mt-3">¿Ya tienes un negocio de repuestos?</p>
                    <div class="d-flex">
                        <div class="form-check">
                            <input class="form-check-input" id="flexRadioDefault1" type="radio" value="si" name="youhavebusiness"/>
                            <label class="form-check-label" for="flexRadioDefault1">Si</label>
                        </div>
                        <div class="form-check ms-lg-4 ms-4">
                            <input class="form-check-input" id="flexRadioDefault2" type="radio" value="no" name="youhavebusiness" checked=""/>
                            <label class="form-check-label" for="flexRadioDefault2">No</label>
                        </div>
                    </div>
                    <div class="form-floating mt-3 d-flex align-items-center">
                        <div id="recaptcha" class="ms-3"></div>
                        <div>
                            <input class="form-control py-3" style="width: 7rem;" type="text" id="codigo" name="codigo" placeholder="captcha"/>
                        </div>
                    </div>
                    <button class="btn btn-primary mt-lg-4 mt-5" type="submit">Enviar<i class="bi bi-arrow-right ms-2"></i></button>
                </div>
            </form>
        </div>
    </section>
</main>
<script src="js/jquery.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
<script src="js/jquery.validate.js"> </script>
<script src="js/sweetalert2.js"></script>
<script src="js/main.js"></script>
<div class="top-footer">
    <footer class="container py-6">
        <div class="row">
            <div class="col-md-6 col-lg-4 col-12 d-md-block d-flex justify-content-center"><a href="./index.php"><img src="img/logo.svg" alt="Logo"/></a></div>
            <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
                <ul class="nav flex-column">
                    <li class="nav-item mb-2"><a class="nav-link p-0" href="./empresa.php">Empresa</a></li>
                    <li class="nav-item mb-2"><a class="nav-link p-0" href="./productos.php">Productos</a></li>
                    <li class="nav-item mb-2"><a class="nav-link p-0" href="./marcas.php">Marcas</a></li>
                    <li class="nav-item mb-2"><a class="nav-link p-0" href="./contacto.php">Contacto</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
                <ul class="nav flex-column">
                    <li class="nav-item mb-3"><a class="nav-link p-0" href="mailto:ventasmayor@aunovit.com"><img class="me-3" src="icon/email.png" alt="Email" width="25"/>ventasmayor@aunovit.com</a></li>
                    <li class="nav-item mb-3"><a class="nav-link p-0" href="tel:+5491170780416"><img class="me-3" src="icon/audifono.png" alt="Phone" width="25"/>+54 9 11 7078 0416</a></li>
                    <li class="nav-item mb-3"><a class="nav-link p-0" href="https://wa.me/5491168581006"><img class="me-3" src="icon/whatsapp.png" alt="Whatsapp" width="25"/>+54 9 11 6858 1006</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-8 mx-auto mt-2 mt-lg-0"><a class="btn btn-primary mt-lg-4 mt-4" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a><a class="btn btn-outline-primary mt-lg-4 mt-4" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></div>
        </div>
    </footer>
</div>
</body>
</html>