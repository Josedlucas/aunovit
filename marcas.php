
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta charset="utf-8"/>
  <title>Aunovit | Marcas</title>
  <link rel="preconnect" href="https://fonts.googleapis.com"/>
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
  <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&amp;display=swap" rel="stylesheet"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <!-- add favicon-->
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png"/>
  <link rel="manifest" href="img/favicon/site.webmanifest"/>
  <body>
    <header>
      <!---->
      <nav class="navbar navbar-expand-md fixed-top bg-white py-4 py-lg-2 border-bottom">
        <div class="container"><a class="navbar-brand" href="./index.php">
            <embed src="img/logo.svg" type="image/png" alt="Logo"/></a>
          <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="nav nav-pills align-items-center d-lg-flex">
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./empresa.php">Empresa</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./productos.php">Productos</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link active" href="./marcas.php">Marcas</a></li>
              <li class="nav-item me-lg-2 position-relative"><a class="nav-link" href="./contacto.php">Contacto</a></li>
              <li class="nav-item me-lg-2 me-md-3 mt-4 mt-lg-auto ms-3"><a class="btn btn-outline-primary" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></li>
              <li class="nav-item mt-4 mt-lg-auto ms-3"><a class="btn btn-primary" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <main>
      <section class="mascas-one"><img class="w-100 d-none d-lg-block" src="img/marcas/section-one.png"/><img class="w-100 d-lg-none d-block" src="img/marcas/section-one-mobile.png"/>
        <div class="container mt-lg-n8 mt-n7">
          <div class="owlMarcas owl-carousel">
            <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4 px-lg-4 py-lg-4 px-3 my-3">
              <div class="py-lg-5 py-4"><img class="card-img-top rounded-top-4 w-50 mx-auto" src="img/marcas/taxim.png" alt="..."/></div>
              <div class="card-body">
                <h5 class="card-title text-primary">Repuestos exclusivos para Renault</h5>
                <div class="mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
                <p class="card-text fs-6">Encontrándose en más de 300 mostradores especialistas de la marca a lo largo y ancho del país, Taxim nace en 2016 trayendo soluciones, buen precio y estándares de altísima calidad en cuanto a cualquier competencia aftermarket.</p>
              </div>
            </div>
            <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4 px-lg-4 py-lg-4 px-3 my-3">
              <div class="py-lg-3 py-3"><img class="card-img-top rounded-top-4 w-50 mx-auto" src="img/marcas/magal.png" alt="..."/></div>
              <div class="card-body">
                <h5 class="card-title text-primary">Variedad en autopartes, alineada a tus necesidades</h5>
                <div class="mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
                <p class="card-text fs-6">
                  La marca más variada en el mundo aftermarket.
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                </p>
              </div>
            </div>
            <div class="card border-0 col-lg-11 my-lg-4 rounded-4 mx-4 mx-md-4 px-lg-4 py-lg-4 px-3 my-3">
              <div class="py-lg-5 py-4"><img class="card-img-top rounded-top-4 w-50 mx-auto" src="img/marcas/nimax.png" alt="..."/></div>
              <div class="card-body">
                <h5 class="card-title text-primary">Repuestos exclusivos para Volkswagen</h5>
                <div class="mb-lg-4 mt-lg-3 mt-3 mb-4"></div>
                <p class="card-text fs-6">
                  La línea más confiable de repuestos para Volkswagen.
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                  <br class="d-none d-lg-block"/>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="marcas-two">
        <h1 class="text-center text-primary pt-lg-3 d-none d-md-block fw-light mt-lg-5">Nuestro stock permanente va de la mano de las <br/>
          mejores marcas nacionales e internacionales.
        </h1>
        <div class="container pt-lg-5 d-none d-md-flex mx-auto row">
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-3" src="img/marcas/taxim.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-3" src="img/marcas/magal.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-3" src="img/marcas/nimax.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-3" src="img/marcas/vmg.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-3" src="img/marcas/hutchinson.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-3" src="img/marcas/spj.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-3" src="img/marcas/fadecya.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-3" src="img/marcas/fremeg.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-3" src="img/marcas/cauplas.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-3" src="img/marcas/corteco.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-3" src="img/marcas/capemi.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-3" src="img/marcas/vth.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-3" src="img/marcas/eyquem.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-3" src="img/marcas/kobla.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-3" src="img/marcas/omer.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-3" src="img/marcas/glacer.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-3" src="img/marcas/airtex.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-3" src="img/marcas/thompson.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/spicer.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/purflux.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/denso.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/duncan.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/litton.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/trico.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/mte-thomson.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/rm.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/Pulo-01.png" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/FREMEC.jpg" width="120"/></div>
          <div class="col-lg-2 d-flex align-items-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-3" src="img/marcas/Fitam-01ok.png" width="120"/></div>
        </div>
        <div class="container pt-lg-5 d-block d-md-none">
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-4" src="img/marcas/taxim.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-4" src="img/marcas/magal.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-4" src="img/marcas/nimax.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-4" src="img/marcas/vmg.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-4" src="img/marcas/hutchinson.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-2s"><img class="mx-4 my-4" src="img/marcas/spj.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-4" src="img/marcas/fadecya.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-4" src="img/marcas/fremeg.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-4" src="img/marcas/cauplas.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-4" src="img/marcas/corteco.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-4" src="img/marcas/capemi.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-3s"><img class="mx-4 my-4" src="img/marcas/vth.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-4" src="img/marcas/eyquem.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-4" src="img/marcas/kobla.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-4" src="img/marcas/omer.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-4" src="img/marcas/glacer.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-4" src="img/marcas/airtex.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-4s"><img class="mx-4 my-4" src="img/marcas/thompson.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/spicer.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/purflux.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/denso.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/duncan.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/litton.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/trico.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/mte-thomson.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/rm.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/Pulo-01.png" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/FREMEC.jpg" width="200"/></div>
          <div class="col-12 d-flex align-items-center justify-content-center animate__animated animate__bounce animate__fadeIn animate__delay-5s"><img class="mx-4 my-4" src="img/marcas/Fitam-01ok.png" width="200"/></div>
        </div>
      </section>
    </main>
    <script src="js/jquery.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
    <script src="js/jquery.validate.js"> </script>
    <script src="js/main.js"></script>
    <div class="top-footer">
      <footer class="container py-6">
        <div class="row">
          <div class="col-md-6 col-lg-4 col-12 d-md-block d-flex justify-content-center"><a href="./index.php"><img src="img/logo.svg" alt="Logo"/></a></div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./empresa.php">Empresa</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./productos.php">Productos</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./marcas.php">Marcas</a></li>
              <li class="nav-item mb-2"><a class="nav-link p-0" href="./contacto.php">Contacto</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-3 col-8 mx-auto mt-4 mt-lg-0">
            <ul class="nav flex-column">
              <li class="nav-item mb-3"><a class="nav-link p-0" href="mailto:ventasmayor@aunovit.com"><img class="me-3" src="icon/email.png" alt="Email" width="25"/>ventasmayor@aunovit.com</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="tel:+5491170780416"><img class="me-3" src="icon/audifono.png" alt="Phone" width="25"/>+54 9 11 7078 0416</a></li>
              <li class="nav-item mb-3"><a class="nav-link p-0" href="https://wa.me/5491168581006"><img class="me-3" src="icon/whatsapp.png" alt="Whatsapp" width="25"/>+54 9 11 6858 1006</a></li>
            </ul>
          </div>
          <div class="col-lg-2 col-8 mx-auto mt-2 mt-lg-0"><a class="btn btn-primary mt-lg-4 mt-4" href="./login.php">Acceso Clientes<i class="bi bi-arrow-right ms-2"></i></a><a class="btn btn-outline-primary mt-lg-4 mt-4" href="./catalogo.php">Catálogo<i class="bi bi-arrow-down ms-2"></i></a></div>
        </div>
      </footer>
    </div>
  </body>
</html>